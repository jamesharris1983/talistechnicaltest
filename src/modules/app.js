var app = angular.module('app', ['app.service']);

app.config = {
	institutions : [
	{
		rootUrl:'http://lists.lib.mmu.ac.uk/',
		listUrl:'http://lists.lib.mmu.ac.uk/lists/',
		moduleUrl:'http://lists.lib.mmu.ac.uk/units/',
		courseUrl:'http://lists.lib.mmu.ac.uk/units/',
		deptUrl:'http://lists.lib.mmu.ac.uk/index.json',
		itemsUrl: 'http://lists.lib.mmu.ac.uk/items/',
		departmentUrl: 'http://lists.lib.mmu.ac.uk/departments/',
		displayName : 'Manchester Metropolitan University'
	},
	{
		rootUrl:'http://lists.lib.keele.ac.uk/',
		listUrl:'http://lists.lib.keele.ac.uk/lists/',
		moduleUrl:'http://lists.lib.keele.ac.uk/modules/',
		courseUrl:'http://lists.lib.keele.ac.uk/courses/',
		deptUrl:'http://lists.lib.keele.ac.uk/index.json',
		itemsUrl: 'http://lists.lib.keele.ac.uk/items/',
		departmentUrl: 'http://lists.lib.keele.ac.uk/schools/',
		displayName : 'Keele University'
	}
	]
};

app.schemaMapping = {
	orgUnit:'http://purl.org/vocab/aiiso/schema#organizationalUnit',
	type:'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
	course:'http://purl.org/vocab/aiiso/schema#Course',
	module:'http://purl.org/vocab/aiiso/schema#Module',
	list:'http://purl.org/vocab/resourcelist/schema#usesList',
	name:'http://purl.org/vocab/aiiso/schema#name',
	code:'http://purl.org/vocab/aiiso/schema#code'
};

/*
http://lists.lib.keele.ac.uk/index.json
[http://purl.org/vocab/aiiso/schema#organizationalUnit]
http://lists.lib.keele.ac.uk/schools/{ou}.json
[http://lists.lib.keele.ac.uk/courses/{0}
(http://www.w3.org/1999/02/22-rdf-syntax-ns#type = http://purl.org/vocab/aiiso/schema#Course)]
http://lists.lib.keele.ac.uk/courses/{0}.json
[http://lists.lib.keele.ac.uk/modules/{0} (http://www.w3.org/1999/02/22-rdf-syntax-ns#type = http://purl.org/vocab/aiiso/schema#Module)]
[http://purl.org/vocab/resourcelist/schema#usesList]
*/

app.selectedInstitution = app.config.institutions[1];