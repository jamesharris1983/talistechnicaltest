angular.module('app.service',['ngResource']).
	factory('list', createResourceFactory(getSelectedInstitutionUrl('listUrl'), 'module')).
	factory('course', createResourceFactory(getSelectedInstitutionUrl('courseUrl'), 'course')).
	factory('ou', createResourceFactory(getSelectedInstitutionUrl('rootUrl'))).
	factory('departments', createResourceFactory(getSelectedInstitutionUrl('departmentUrl'), 'department'))
;

function getSelectedInstitutionUrl(url){
	return function(institution) {
		return institution[url];
	};
}

function createResourceFactory(url, idkey) {
	return function($resource) {
		var urlval;
		if (idkey){
			urlval = url(app.selectedInstitution) + ':' + idkey + '.json'
		} else {
			urlval = url(app.selectedInstitution) + '.json'
		}

		return $resource(urlval,
			{alt:'json', cb:'JSON_CALLBACK'}, {
				query: { method: 'JSONP', responseType:'json' }
			});
	};
}