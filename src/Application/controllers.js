var cb =  { params : { cb:'JSON_CALLBACK' }};

app.controller( 'moduleController', ['$scope', '$http', function($scope, $http, list, ou, departments, course) {
	$scope.selectCourse = function() {
		getModules($scope, $http);
	};
	$scope.selectInstitution = function() {
		app.selectedInstitution = $scope.institution;
		$scope.selectedInstitution = app.selectedInstitution.displayName;
		getDepartments($scope, $http);
	};
	$scope.selectDepartment = function(){
		getCourses($scope, $http);
	};
	$scope.selectModule = function(){
		getLists($scope, $http);
	};
	$scope.institutions = app.config.institutions;
	$scope.selectedInstitution = app.selectedInstitution.displayName;

	getDepartments($scope, $http);
}]);

function getDepartments($scope, $http){
	$http.jsonp(app.selectedInstitution.deptUrl, cb).then(function(response){
		var data = response.data;
		var root = data[app.selectedInstitution.rootUrl];
		var departments = root[app.schemaMapping.orgUnit];
		$scope.departments = _.map(departments, function(val){
			return follow($http, val.value, {
				code : app.schemaMapping.code,
				name : app.schemaMapping.name
			});
		});
	});
}

function getCourses($scope, $http){
	$http.jsonp(app.selectedInstitution.departmentUrl + $scope.department.code.toLowerCase() + '.json', cb).then(function(response){
		var data = response.data;
		$scope.courses = _.map(_.keys(data), function(field){
			if (field.indexOf(app.selectedInstitution.courseUrl) != -1) {
				return {
					code : data[field][app.schemaMapping.code][0].value,
					name : data[field][app.schemaMapping.name][0].value
				};
			}
		});
	});
}

function getModules($scope, $http){
	$http.jsonp(app.selectedInstitution.courseUrl + $scope.course.code.toLowerCase() + '.json', cb).then(function(response){
		var data = response.data;
		$scope.modules = _.map(_.keys(data), function(field){
			if (field.indexOf(app.selectedInstitution.moduleUrl) != -1){
				var usesListProp = data[field][app.schemaMapping.list];

				var result = {
					name : data[field][app.schemaMapping.name][0].value
				};

				if (usesListProp) {
					result.usesList = usesListProp[0].value;
				}

				return result;
			}
		});
	});
}

function getLists($scope, $http) {
	var url = $scope.module.usesList;

	$scope.list = $http.jsonp(url + '.json', {params: {cb:'JSON_CALLBACK'}}).then(function(response) {
		var data = response.data;
		var root = data[url];
		$scope.title = root['http://rdfs.org/sioc/spec/name'][0]['value'];
		var descriptionNode = root['http://purl.org/vocab/resourcelist/schema#description'];

		if(descriptionNode) {
			$scope.description = [0]['value'];
		}

		return _.map(_.filter(root['http://purl.org/vocab/resourcelist/schema#contains'],
		function(val){
			return(val.value.indexOf(app.selectedInstitution.itemsUrl) !== -1);
		}),
		function(val){
			return follow($http, val.value,
				{
					resource :
					{
						name : 'http://purl.org/vocab/resourcelist/schema#resource',
						data:
						{
							title : 'http://purl.org/dc/terms/title'
						}
					}
			});
		});
	});
}

function follow($http, url, data){
	return $http.jsonp(url + '.json', {params: {cb:'JSON_CALLBACK'}}).then(function(response){
		if (response.status === 200) {

			var result = { url: url };

			for(var field in data){
				if (typeof data[field] === "object") {
					var parent = response.data[url][data[field].name][0]['value'];
					for(var child in data[field].data){
						result[child] = response.data[parent][data[field].data[child]][0]['value'];
					}
				} else {
					result[field] = response.data[url][data[field]][0]['value'];
				}
			}

			return result;
		}
	});
}
